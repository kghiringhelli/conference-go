from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_pics(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    r = requests.get(url, params=params, headers=headers)
    content = json.loads(r.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather(city, state):
    url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {"q": city + "," + state + "USA", "appid": OPEN_WEATHER_API_KEY}
    r = requests.get(url, params=params)
    content = json.loads(r.content)
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    url1 = "https://api.openweathermap.org/data/2.5/weather"
    params1 = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    r1 = requests.get(url1, params=params1, headers=headers)
    content1 = json.loads(r1.content)
    try:
        return {
            "weather": {
                "temp": content1["main"]["temp"],
                "description": content1["weather"][0]["description"],
            }
        }
    except (IndexError, KeyError):
        return {"weather": None}
